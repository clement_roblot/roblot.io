#!/bin/bash

BASEDIR=$(dirname "$0")

cd $BASEDIR

if grep -q push "push.md"; then
    echo "we need to push"

    # Empty the content of the file (prevent further re-triggering)
    > push.md

    # push the changes
    git add -A
    git commit --all -m "Automatic commit"
    git pull
    git push
fi


