#!/bin/bash

BASEDIR=$(dirname "$0")

cd $BASEDIR

bundle exec jekyll serve &

sleep 3

xdg-open http://127.0.0.1:4000/
