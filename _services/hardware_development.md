---
layout: service
title: "Hardware design"
type: "hardware"
short_description: "PCB design and its integration in a mechanical system."
icon: "/assets/images/techIcons/AI-Artificial Intelligence.png"
post_image: "/assets/images/services/pcb.jpg"
order: 1
---

We have the design capability to design systems utilising all the major modern signals from I2C to ethernet. We do not have capabilities in microwave and very high speed signals.

