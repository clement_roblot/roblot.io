---
layout: project
title: "Livecore from Analog Way"
category: "embedded"
type: "Industrial"
icon: "/assets/images/projects/livecoreScreen.jpg"
post_image: "/assets/images/projects/livecore.png"
---

Analog Way is a company that specializes in the design and manufacture of professional video and image processing equipment. They provide a range of solutions for various industries, including live events, rental and staging, broadcast, corporate, and more. Their products include video mixers, seamless switchers, scalers, routers, and control systems that are used to enhance the visual and audio experience of live presentations, events, and broadcasts. Analog Way's mission is to provide innovative, reliable, and user-friendly solutions to the professional audiovisual market.

I worked on the livecore series of devices: a seamless switcher between up to 12 input and 4 output with the ability to link two of them together effectively doubling its capabilities.

Those systems are constituted of a series of custom PCB containing different frontend and backends to format the video signals going in and out, multiple stages of FPGAs to actually do the image processing and finally the part I was working on: the microcontrolers that configure all those components.

The work in this project isn't about the actually mathematical image processing, but in generating the right configurations to send to the frontends, FPGAs and backends to realize the processing the user requested. Which such a high number of components and so many variables the main difficulty in such a project is to keep the code well organized to keep things understandable as the project grows.

Of course in such a long running project, the size of the codebase and of the memory footprint are bound to become an issue sooner or later. Luckily in this case, some optimizations when it comes to structure packing allowed me to avoid any blocking situations.
