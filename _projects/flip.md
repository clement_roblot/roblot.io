---
layout: project
title: "The fl!p"
category: "embedded"
type: "Handheld IOT"
icon: "/assets/images/projects/theFlip.png"
post_image: "/assets/images/projects/theFlip.png"
---

Quizflip is a French startup envisioning to revolutionize the way we learn by designing a digital flash card using 2 e-ink displays mounted back-to-back.

Such a device requires a lot of embedded software development, it is in that functions that they asked me to give them a hand.

A few improvements were made to the already existing systems like for instance the movement detection system making it more robust though the use of better signal processing logics. Then implementing features one by one allowed us to develop a strong product with the attended simplicity of use allowing to user to focus on the learning. Those features ranges from being able to display a fashcard from the SD card to playing some audio also stored on the SD card to being able to write on the display, meaning being able to generate custom text rendered by the microcontroler itself.

Being involved at every stages of the project from the embedded side of the device to the front end mobile app side in react native allowed me to have a vision over the protocols and the way every parts communicates allowing fast development and to make choices in the best interest of the project. For instance by delegating intensive calculations out of the embedded device and onto the smartphone.
