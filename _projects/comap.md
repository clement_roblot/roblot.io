---
layout: project
title: "COMAP Smart Home"
category: "embedded"
type: "Domotic IOT"
icon: "/assets/images/projects/comapHead.jpg"
post_image: "/assets/images/projects/comap.png"
---

COMAP is one of the French leaders of piping production and industrial control. They develop a range of products targeted towards different industries ranging from industrial and maritime to personnel usage.

For the development of their new range of personnel products, they needed to add a security layer protecting the communications between the different embedded elements across the house of the user.

This security layer had to be implemented in a very constrained environment with very little memory, very little processing power and using as little energy as possible to let the embedded batteries last as long as possible.

My expertise in embedded systems allowed me to devise a technique for encrypting the communications that used the least amount of resources in the system. The usage of open source security libraries helped us build a system that had a track record of stability instead of starting the work from scratch allowing the product to be ready quickly on the security side of things.
