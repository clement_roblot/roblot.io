---
layout: post
title:  "Using python in a webapp"
date:   2021-05-16 09:16:35 +0600
tags: python webapp
categories: [Python]
post_image: "/assets/images/blog/pythonWebapp.png"
author: Clément Roblot
---

We have a look at what goes into making a webpage able to call a python script. As it turns out the security requirements makes it so that it requires quite a few layers of script:

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/60_09647m9E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
