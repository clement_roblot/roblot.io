---
layout: post
title:  "How to cheaply program a BMD-340 module using an st link V2"
date:   2021-02-04 09:16:35 +0600
tags: BMD-340 st-link-V2
categories: [Embedded]
post_image: "/assets/images/blog/BMD-340.png"
author: Clément Roblot
---

Trying to build a project without having to spend hundreds of dollars on gear that I wouldn’t need long term (namely for a j-link programmer) I had to figure out how to program my BMD-340 modules using what I had: an st link V2.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/T7MI0L0TNgk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
