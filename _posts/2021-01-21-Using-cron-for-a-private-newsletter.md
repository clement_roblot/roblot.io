---
layout: post
title:  "Using cron for a private newsletter"
date:   2021-01-21 09:16:35 +0600
tags: Email Automation Python
categories: [System]
post_image: "/assets/images/blog/Cron-Newspaper.png"
author: Clément Roblot
---
Having had issues spending too much time on news website I decided to build myself a private newsletter that will need to be sent every days. Instead of using a server running 24/7 sending the email every day at midnight I built a system using cron and some python code to ensure the daily sending of the email without the need for my laptop to be turned on at the right time.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/-yrMtBn4gmU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
