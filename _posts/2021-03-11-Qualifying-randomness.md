---
layout: post
title:  "Qualifying randomness | RNG Part 1"
date:   2021-03-11 09:16:35 +0600
tags: C random
categories: [Embedded]
post_image: "/assets/images/blog/qualifyingRandomness.png"
author: Clément Roblot
---

Just started a series of videos explaining how to generate good random numbers in embedded systems without the need for a True Random Number Generator. In this first part, we have a look at how to qualify randomness and what makes good random.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/GDCmvb5F4gU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
