---
layout: post
title:  "Arduino like a pro"
date:   2021-02-18 09:16:35 +0600
tags: Arduino Makefile
categories: [Embedded]
post_image: "/assets/images/blog/Arduino.png"
author: Clément Roblot
---

I do not like using the arduino IDE at all but I love saving time by not having to re-invent the wheel. In this video we see how to take advantage of the arduino libraries without having to endure the IDE that comes with it.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/ASzHKYIwzOA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
