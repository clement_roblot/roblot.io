---
layout: post
title:  "Solve frame rate issue of logitech C920 on OBS"
date:   2020-05-11 09:16:35 +0600
tags: C920 OBS
categories: [System]
post_image: "/assets/images/blog/c920.jpeg"
author: Clément Roblot
---


<iframe width="100%" height="500px" src="https://www.youtube.com/embed/UCM3hAPdA_U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You just installed OBS and connect to your shiny new Logitech C920 to realize that the frame rate is garbage. This article will explain to you how to solve that issue.

## Too long didn’t read

Simply select the video format of “_YV12 (emulated)_” to get the promised 30fps at full HD resolution.

![OBS screenshot](/assets/images/blog/obs-1.png)

## Explanation

To the best of my knowledge, this issue comes from the fact the YV12 is a 4:2:0 sampling in opposite to a regular YUV which would be a 4:2:2 sampling with OBS. That switch from 4:2:2 to 4:2:0 seams to free enough bandwidth to allow for the promised 30fps.

More info about sampling can be found [here](http://avisynth.nl/index.php/Sampling#The_color_formats:_RGB.2C_YUY2_and_YV12).