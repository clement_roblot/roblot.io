---
layout: post
title:  "How to get rid of youtube slow buffering"
date:   2020-05-18 09:16:35 +0600
tags: buffering C++ Qt youtube
categories: [System]
post_image: "/assets/images/blog/localtube.png"
author: Clément Roblot
---


<iframe width="100%" height="500px" src="https://www.youtube.com/embed/1-22nYSIzNo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

When I was living in France my ISP had a lot of issues connecting to YouTube especially in the evening when everybody’s coming back from work. This lead to a lot of buffering issues. To remedy those issues I devised a technique that would allow me to watch the videos in Wanted but without any buffering/lag issues while watching the video

Localtube is designed to connect to the YouTube API fetch the list of all the channels you subscribe to fetch all the latest videos from social channels and download them one by one. It may require hours to download one video but once it’s done it is saved on your hard drive and thus will not have any buffering problems.

I designed this application using C++ and Qt allowing for a snappy interface. In hindsight nowadays I might be doing it in Python once again using Qt to reduce the time of development. The resulting interface might be a little bit slower but that should not be an issue for such an application.

You can download Localtube from the website [here](https://localtube.gitlab.io/website/) and the source code is open-source and available [here](https://gitlab.com/localtube/localtube).