---
layout: post
title:  "How to export really full page PDF in Altium designer"
date:   2021-02-11 09:16:35 +0600
tags: Altium Export PDF
categories: [Hardware]
post_image: "/assets/images/blog/Atium.png"
author: Clément Roblot
---

I’m annoyed to see soo many PCB projects not using the full page when doing an export of schematics in Altium designer. Here is a quick tutorial on how to get clean full page pdfs in your Altium Designer exports.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/TyNaMRh3ecQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
