---
layout: post
title:  "Battery chargers teardown"
date:   2021-04-07 09:16:35 +0600
tags: Charger
categories: [Teardown]
post_image: "/assets/images/blog/batteryTeardown.png"
author: Clément Roblot
---

Finding out what is inside of a battery charger and investigating why one of them never seems to finish charging:

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/2n3-MwJh5aw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
