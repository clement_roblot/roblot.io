---
layout: post
title:  "Bus size and performances"
date:   2021-04-23 09:16:35 +0600
tags: 32bits MCU
categories: [Embedded]
post_image: "/assets/images/blog/busSizes.png"
author: Clément Roblot
---

Does doubling the number of bits of a microcontroller double its processing power? What does it mean this number of bits?

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/VWRtFE7qAw4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
