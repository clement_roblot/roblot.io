---
layout: post
title:  "Counting to the end of the world"
date:   2021-05-28 09:16:35 +0600
tags: 32bits counting
categories: [Embedded]
post_image: "/assets/images/blog/counting.jpg"
author: Clément Roblot
---

How big of a number is 64bit? Quite big as it turns out:

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/NRoHE2uXlbw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
