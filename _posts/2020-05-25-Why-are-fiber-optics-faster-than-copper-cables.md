---
layout: post
title:  "Why are fiber optics faster than copper cables (it is not the speed of light)"
date:   2020-05-25 09:16:35 +0600
tags: Fiber Laser
categories: [Physics]
post_image: "/assets/images/blog/fiberOptics.jpg"
author: Clément Roblot
---


<iframe width="100%" height="500px" src="https://www.youtube.com/embed/9eJr8yKV1E4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We have been told by marketing and advertisement that fiber optic gives us faster Internet thanks to the speed of light. As it turns out it is a lie. ????

## What happens in a copper cable

The easiest way to see that issue is to check the speed at which electrons move in copper. If we measure it we get a result of around 10 km an hour this is slow but it’s quite obvious that the Internet is not running on a system that can move data at a speed of 10 km an hour something is wrong with that assumption. This would means that sending data from one side of the earth to the other would take around 4000 hours. Obviously the Internet is not running with such a technology.

In that case, we have assumed that data is moved by moving electrons. Sending a “1” is represented by sending electrons and a “0” is represented by sending no electrons it turns out to not be how the internet and transmission of data works.

## Waves

In reality, data is transferred with what is similar to a wave. We are going to drive current into the copper by pushing electrons in one end of the cable which is going to almost immediately get the electrons on the other side of the cable to pop out. That process is very similar to [Newton’s cradle](https://en.wikipedia.org/wiki/Newton%27s_cradle). In things example with metal balls, the transfer of energy is instantaneous with virtually no dampening effect. that makes the process of moving information or energy, in that case, seemingly instantaneous. If we replace the metal balls by electrons (with electromagnetics and other atomic forces adding some dampening effect) and measure the speed at which the information is transferred we get a speed of around 200,000 km a second. That’s means sending data to the other side of the earth takes around 100ms which makes more sense.

## What about fiber optics

We’ve been told in high school that the speed of light is 300,000 km a second which is significantly higher than the speed of a wave of energy in a copper cable that we just saw earlier (200,000 km). Well as it turns out this also is wrong that number of 300,000 km a second is for the vacuum of space and it is assumed to be close enough for in free air. Issues comes from the fact that fiber optics are not made of vacuum nor air, they are made of plastic and glass those have an index of refraction of 1.4 [which effectively means that the light is going through them at the lower speeds than the theoretical speed of light](https://en.wikipedia.org/wiki/Refractive_index). 300,000÷1.4 equals 200,000 km/s. We end up with a very similar speed for transferring light in a fiber optic and the speed of transferring energy in a copper cable. That raises the question : why then is fiber-optic definitely faster than copper cable

## Data encoding

It all comes down to how is the data encoded on the cables. You can send zeros and ones by sending Energy or not sending energy, but that would be very un-efficient you could compress the data to send more of it in a given timeslot. Some bits of compressed data are called symbols and a list of all the symbols that you are using to transfer information is an alphabet. Having a bigger alphabet allows you to compress data more and thus send that data more efficiently. The downside of a bigger alphabet is that every symbols are going to be closer to each other which leads to detection issues.

If you use frequency to encode a symbol the first symbol is going to have the lowest frequency and the last symbol is going to have the highest frequency. The number of subdivisions a.k.a. the number of symbols in your alphabet is going to determine the step in between two frequencies. It is also going to define how sensitive your input is going to have to be on the other end of the cable. The issue comes from the fact that your input cannot be infinitely precise if you add on top of that the fact that you have noise and distortions of the signal by resistance in the cable your data signal is going to come out dirty wich is going to make it harder to read. That’s why you cannot have an infinite size for your alphabet and that is also where the big differences between copper cable and fiber optics arise. Copper cables are a lot more sensitive to noise and a lot more resistive than fiber optics which means that the alphabets that you used to encode your data on copper cables are going to have to be a lot smaller than the one you can use we fiber optics.

## Conclusion

The speed gain from fiber-optic has nothing to do with the speed of light per se. It has everything to do with attenuation and the way to encode and compressed data. The lower attenuation allows us to cram more data into a given timeslot and that is the reason why we can send a lot more data faster on a fiber optic than on a copper cable.