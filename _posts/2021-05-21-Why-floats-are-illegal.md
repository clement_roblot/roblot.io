---
layout: post
title:  "Why floats are illegal"
date:   2021-05-21 09:16:35 +0600
tags: float mcu
categories: [Embedded]
post_image: "/assets/images/blog/softFloat.png"
author: Clément Roblot
---

Let's see why floats are traditionally avoided in embedded systems. In reverse in a system that does use them, removing them is a very good opportunity to save space when needed.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/fEpuJJDCBKk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
