---
layout: post
title:  "Multitasking without an OS"
date:   2021-05-03 09:16:35 +0600
tags: OS task
categories: [Embedded]
post_image: "/assets/images/blog/multitasking.png"
author: Clément Roblot
---

Let's see how to have multiple tasks inside of our micro controller without having to install an OS in it.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/iMtPmnzukeE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
