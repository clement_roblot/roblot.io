---
layout: post
title:  "Building a private newsletter"
date:   2020-06-01 09:16:35 +0600
tags: Automation Mail Python
categories: [Physics]
post_image: "/assets/images/blog/newsletter.png"
author: Clément Roblot
---
After a few months of subscribing to way too many newsletters and reading way too many articles on the Internet, I realize I had to change my way of consuming content to not spend my entire life-consuming. That is when I decided to build a software designed to make the choices of what I will be allowed to consume and ignore the rest (as one does).

This software will be in the form of a daily newsletter that is going to merge different sources of information and build myself a daily email containing all the information that I am allowed to read for today and nothing more.

For the first version, I kept things simple and only took the daily most popular articles from hackernews, and put it in a nice template mostly inspired by the newsletter “the elevator”. I used python to make this processed as fast as possible to develop.

In the first place the system needs to connect to the API of hacker news and get the most popular articles. I used a package to fetch the first image from the article (extraction) and another one to make a summary of the article (summarizers).

I have now been collecting quotes for a number of years and in order to review them from time to time, I will add one selected at random per day in the email.

Combining all of that into an HTML template then automatically sending that email every day to myself allows me to drastically reduce the amount of times that I can spend on the news website.

The source code for this mailing list application is open source and available [here](https://github.com/clement-roblot/newsletter).

![Newsletter example](/assets/images/blog/newletterExample.png)
