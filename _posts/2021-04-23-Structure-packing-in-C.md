---
layout: post
title:  "Structure packing in C | How to get memory for free"
date:   2021-04-23 09:16:35 +0600
tags: C Structure
categories: [Embedded]
post_image: "/assets/images/blog/structurePacking.png"
author: Clément Roblot
---

In 100% of the embedded projects I worked on, memory became an issue at some point. Either not enough RAM or not enough flash. Here we see an easy way to optimize our data structures in order to save space in memory at no cost of performance!

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/Eidz1Uh7-pk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
