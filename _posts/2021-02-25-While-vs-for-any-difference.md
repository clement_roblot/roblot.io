---
layout: post
title:  "While(1) vs for(;;) any difference ?"
date:   2021-02-25 09:16:35 +0600
tags: C embedded
categories: [Embedded]
post_image: "/assets/images/blog/Infinit-Loops.png"
author: Clément Roblot
---

I see more and more the for(;;) being used as an infinite loop, I was told it is a better way of doing an infinite loop but I am not sure why. We will see what is the difference between the two.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/aF_XwLDcIOw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
