---
layout: post
title:  "Working with AC main safely"
date:   2021-01-18 09:16:35 +0600
tags: AC Safety
categories: [System]
post_image: "/assets/images/blog/mainSafety.png"
author: Clément Roblot
---
Designing electronics that takes the AC main as a power source is great, for once there is no battery to manage and you can use basically as much power as you want. But you also need to be careful to not electrocute yourself.

In this video, I explain what I use to develop high voltage electronic systems safely.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/myDIA7tUVPg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
