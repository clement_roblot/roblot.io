---
layout: post
title:  "Going over the bus size"
date:   2021-05-17 09:16:35 +0600
tags: 32bits 16bits
categories: [Embedded]
post_image: "/assets/images/blog/overBus.png"
author: Clément Roblot
---

Let's do the experiment of doing calculations bigger than the bus size of a modern microcontroller and see how bad the generated assembly code look.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/jByDEDshGuc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
