---
layout: post
title:  "Mixing and merging random sources | RNG Part 2"
date:   2021-03-17 09:16:35 +0600
tags: C random
categories: [Embedded]
post_image: "/assets/images/blog/merginRandom.jpg"
author: Clément Roblot
---

Today we keep looking into how to generate random numbers in an embedded system by acquiring random information and having a look at how to merge the randomness of multiple sources into one.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/ACqwR872Zik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
