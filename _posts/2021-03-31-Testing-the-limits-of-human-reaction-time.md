---
layout: post
title:  "Testing the limits of human reaction time"
date:   2021-03-31 09:16:35 +0600
tags: humans
categories: [System]
post_image: "/assets/images/blog/Human-Reaction.jpg"
author: Clément Roblot
---

How slow humans look as seen by a microcontroller. In this video, we test how much time a human takes to react to an input coming from a microcontroller.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/euoCuGQIUeE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
