---
layout: post
title:  "Embedded Mersenne Twister | RNG Part 3"
date:   2021-03-26 09:16:35 +0600
tags: C random
categories: [Embedded]
post_image: "/assets/images/blog/mersenneTwister.png"
author: Clément Roblot
---

This time we have a look at how to leverage the true random we generated using a Mersenne Twister to generate a lot more random numbers.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/c6G2_5ONZCo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
